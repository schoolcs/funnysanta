﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inputManager : MonoBehaviour {
	Weapon pistol;
	public float speed;
	public bool isCollide;
	// Use this for initialization
	void Start () {
		pistol = new Weapon ();
		pistol.typeW = "Огнестрельное";
		pistol.weight = 0.5f;
		pistol.numberOfBullets = 12;
	}
	
	// Update is called once per frame
	void Update () {
		float inputVertical;
		float inputHorizontal;

		inputVertical = Input.GetAxis ("Vertical");

		inputHorizontal = Input.GetAxis ("Horizontal");

		if (isCollide != true) {
			if (inputVertical != 0) {
				// перемещаем объект по оси z вверх или вниз
				transform.Translate (0, 0, inputVertical * speed);
			}

			if (inputHorizontal != 0) {
				// перемещаем объект по оси x вправо или влево
				transform.Translate (inputHorizontal * speed, 0, 0);
			}
		} else {

		}
		Debug.Log (inputVertical);
	}

	void OnTriggerEnter(Collider other) {
		isCollide = true;
	}
}



public class Weapon {
	public string typeW;
	public float weight;
	public int numberOfBullets;
	public string image;
}