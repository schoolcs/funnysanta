﻿/*

Скрипт предназначен для задержки перед удалением игрового объекта

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class delayEffect : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(waiter());
        /*
         Читаем тут про этот метод https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html 
        */

        /*
           Выполнение подпрограммы можно приостановить в любой момент, используя инструкцию yield. 
           Возвращаемое значение yield указывает, когда возобновить подпрограмму. 
           Coroutines превосходны при моделировании поведения в нескольких кадрах. 
           Функция StartCoroutine всегда возвращается немедленно, однако вы можете задержать результат. 
           yield будет ждать завершения выполнения команды. 
           Нет гарантии, что подпрограммы заканчиваются в том же порядке, в котором они были запущены, даже если они заканчиваются в одном кадре. 

       */

    }

    // Update is called once per frame
    void Update () {

     }

     IEnumerator waiter()
     {
        
        yield return new WaitForSeconds(3); /* приостанавливает выполнение подпрограммы в течение заданного количества секунд с использованием масштабированного времени. */
        Destroy(gameObject); /* уничтожаем игровой объект, на котором скрипт, после задержки */
    }
}
