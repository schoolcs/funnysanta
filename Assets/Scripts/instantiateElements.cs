﻿/*
 
    Скрипт, содержащий метод, высчитывающий местоположение нового объекта рядом с последним
    и ставящий новый объект в сцену
 
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class instantiateElements : MonoBehaviour {
    public GameObject[] objectsArray; // массив объектов для установки в сцену
	public GameObject presentBonus;
    public GameObject instantiatePoint; // пустой объект, в координатах y и z которого будут появляться новые объекты
    public int mainSpeed; // скорость перемещения объектов
    int i=0; // счетчик
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    // метод, вызываемый из скрипта moveObject, параметр метода - тот объект, который его вызывает
    public void SetObjectPosition(GameObject oldObject)
    {

        if (i < objectsArray.Length)
        {
           
            int objectNumber = i; // устанавливаем номер объекта в массиве
            
            GameObject lastObj = GameObject.FindGameObjectWithTag("last"); // находим игровой объект с тегом last
            float maxX = lastObj.GetComponent<Collider>().bounds.max.x; // определяем макс. координату x (правую границу) последнего объекта
            /* Вбрасываем новый игровой объект в сцену из массива.
               Пришлось отказаться от предыдущего метода из-за долгой настройки игровых объектов и
               многочисленных багов при появлении новых объектов. Нет времени разбираться. Используем префабы
             */
            GameObject newObject = Instantiate(objectsArray[objectNumber], instantiatePoint.transform.position, instantiatePoint.transform.rotation);
            float newObjlength = newObject.GetComponent<Collider>().bounds.max.x - newObject.GetComponent<Collider>().bounds.min.x; // определяем длину нового объекта в координатах Юнити по оси x
            float xPos = maxX + (newObjlength / 2 ); // высчитываем местоположение нового объекта рядом с последним
            newObject.transform.position = new Vector3(xPos, instantiatePoint.transform.position.y, instantiatePoint.transform.position.z); // устанавливаем новый объект
            newObject.tag = "last"; // новому объекту присваиваем тег last
            lastObj.tag = "instObject"; // предыдущему объекту присваиваем другой тег
            
            moveObject newOScript = newObject.GetComponent<moveObject>(); // получаем скрипт на новом объекте как компонент
            newOScript.enabled = true; // делаем его активным
            newOScript.speed = mainSpeed; // устанавливаем скорость перемещения
            i++; // увеличиваем счетчик 
            
        } else
        {
            i = 0;
        }
    }

	public void SetBonusPosition() {
		int i=0;
		if (gameManager.presentsBonusCount == 0) {
			Vector3 newPosition = new Vector3 (200, Random.Range (20f, 50f), 5.37f);
			Instantiate (presentBonus, newPosition, presentBonus.transform.rotation);
			gameManager.presentsBonusCount = 1;
		}
	}
}
