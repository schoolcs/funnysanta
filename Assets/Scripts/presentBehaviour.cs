﻿/*

    Скрипт подарков, определяет столкновение подарка и показывает эффекты

*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class presentBehaviour : MonoBehaviour {
	public GameObject effect, explode; // игровые объекты - эффекты
    public GameObject effectPrefab, explodePrefab; // эффекты - префабы
    public float expRadius, expPower; // радиус определения коллайдеров и сила взрыва
    public Text scoreText; // UI - текущий счет
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision other) { // вызывается, если колалйдер соприкоснулся с другим коллайдером
                                             // переменная other содержит другой коллайде вместе с игровым объектом
        gameManager.canPresent = true; // соприкоснулся - значит можно кидать еще подарок
		/*Rigidbody rgBodyPr = gameObject.GetComponent<Rigidbody> ();
		rgBodyPr.velocity = new Vector3 (0, 0, 0);
		rgBodyPr.isKinematic = true;
        */
		if (other.gameObject.tag=="chimney")
        { // если у того объекта, с которым произошло столкновение тег - chimney,
            // то увеличиваем счет и показываем эффект
            effect = Instantiate(effectPrefab, gameObject.transform.position, gameObject.transform.rotation); // вбрасываем эффект из префаба там, где произошло столкновение
			effect.gameObject.transform.parent = other.gameObject.transform; // устанавливаем родителем для эффекта тот объект, с которым произошло столкновение
            // увеличиваем счет
			//int localScore = PlayerPrefs.GetInt ("localScore");
			gameManager.localScore += 100;
			//scoreText.text = "Score: " + localScore.ToString ();
			PlayerPrefs.SetInt ("localScore", gameManager.localScore);
           // изменяем цвет материала на зеленый на том объекте, с которым произошло столкновение
            Renderer rend = other.gameObject.GetComponent<Renderer>();
            rend.material.shader = Shader.Find("Specular");
            rend.material.SetColor("_SpecColor", Color.green);

        } else
        {
            // если столкновение не струбой - то взрыв
            Vector3 explosionPos = other.gameObject.transform.position; // сохраняем позицию столкновения
            // далее получаем коллайдеры объектов, находящихся в радиусе некоторой сферы
            // про метод Physics.OverlapSphere читаем в документации
            // коллайдеры сохраняются в массив colliders
            Collider[] colliders = Physics.OverlapSphere(explosionPos, expRadius);
            // цикл обработки каждого полученного коллайдера
            // текущий коллайдер в цикле присваивается переменной hit
            foreach (Collider hit in colliders)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>(); // получаем компонент Rigidbody

                if (rb != null) { // если Rigidbody был получен
                    rb.isKinematic = false; // убираем isKinematic
                    rb.AddExplosionForce(expPower, explosionPos, expRadius, 3.0F); // добавляем некий force
                    // про AddExplosionForce читаем в документации
                    hit.enabled = false; // убираем enabled коллайдера (делаем его не активным)   
                }
            }
            // ниже код, вбрасывающий эффект взрыва
            /*explode = Instantiate(explodePrefab, gameObject.transform.position, gameObject.transform.rotation);
            explode.gameObject.transform.parent = other.gameObject.transform;*/
        }
        /*
        gameObject.transform.position = new Vector3 (-100, 0, 0);
        gameObject.SetActive(false);
        */
        Destroy(gameObject); // уничтожаем текущий подарок
    }
   
}
