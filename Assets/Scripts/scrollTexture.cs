﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrollTexture : MonoBehaviour {
    public float scrollSpeed  = .5f;
    float off = 0f;
    float rotate;
    Renderer planeRender;
    // Use this for initialization
    void Start () {
        planeRender = gameObject.GetComponent<Renderer>();

    }
	
	// Update is called once per frame
	void Update () {
        off = off + (Time.deltaTime * scrollSpeed) / 10.0f;
        planeRender.material.SetTextureOffset("_MainTex", new Vector2(-off, 0f));
    }
}
