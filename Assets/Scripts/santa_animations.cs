﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class santa_animations : MonoBehaviour {
    Animator santa_animator_controller;
	// Use this for initialization
	void Start () {
        santa_animator_controller = gameObject.GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Keypad1))
        {
            santa_animator_controller.SetInteger("key", 1);
        }

        if (Input.GetKey(KeyCode.Keypad2))
        {
            santa_animator_controller.SetInteger("key", 2);
        }
        if (Input.GetKey(KeyCode.Keypad3))
        {
            santa_animator_controller.SetInteger("key", 3);
        }
        if (Input.GetKey(KeyCode.Keypad0))
        {
            santa_animator_controller.SetInteger("key", 0);
        }
        //Debug.Log(santa_animator_controller.GetCurrentAnimatorStateInfo(0));
        //Debug.Log(AnimatorStateInfo.name);

    }
}
