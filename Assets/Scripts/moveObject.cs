﻿/*

    Скрипт, перемещающий объект слева направо
 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveObject : MonoBehaviour {
    public float speed; // скорость перемещения
    GameObject objectsManager; // в эту переменную будем записывать некий менеджер объектов из сцены
    instantiateElements obmScript; // скрипт установки объектов в сцену
    // Use this for initialization
    void Start () {
        objectsManager = GameObject.Find("objectsManager"); // находим игровой объект с именем objectsManager
        obmScript = objectsManager.GetComponent<instantiateElements>(); // получаем скрипт на нем
        speed = obmScript.mainSpeed; // считываем значение переменной mainSpeed
    }
	
	// Update is called once per frame
	void Update () {
        
        transform.Translate(-speed * Time.deltaTime, 0, 0); // перемещаем объект слева направо
        /* Если объект попадает в координату -90 - вызываем метод установки нового объекта.
           Лучше использовать метод определения попадания правого края объекта за пределы экрана.
        */
        if (transform.position.x<-90)
        {
			if (gameObject.tag == "presentBonus") {
				gameManager.presentsBonusCount = 0;
				if ((gameManager.presentsCount < 4) && (gameManager.presentsBonusCount == 0)) {
					obmScript.SetBonusPosition ();
				}

			}

			if ((gameObject.tag == "last") || ((gameObject.tag == "instObject"))) {
            	obmScript.SetObjectPosition(gameObject); // вызываем метод SetObjectPosition установки нового объекта скрипта instantiateElements
			}
            Destroy(gameObject); // уничтожаем объект
        }
	}
}
