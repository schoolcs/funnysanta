﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveSanta : MonoBehaviour {
	Rigidbody santaRB;
	public float speed = 10;
	public GameObject centerPos;
	public GameObject rightGO;
	public GameObject topGO;
	// Use this for initialization
	void Start () {
		santaRB = gameObject.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		float vert = Input.GetAxis ("Vertical");
		float hor = Input.GetAxis ("Horizontal");

		if (vert != 0 || hor != 0) {
			santaRB.AddForce (hor * speed, vert * speed, 0);
		}

		Vector3 pos = Camera.main.WorldToViewportPoint(centerPos.gameObject.transform.position);

		if (pos.x < 0.0)
			santaRB.velocity = new Vector3 (-santaRB.velocity.x, santaRB.velocity.y, 0);
		
		if(1.0 < pos.x) 
			santaRB.velocity = new Vector3 (-santaRB.velocity.x, santaRB.velocity.y, 0);
		
		if(1.0 < pos.y) 
			santaRB.velocity = new Vector3 (santaRB.velocity.x, -santaRB.velocity.y, 0);
		
	}

	void OnTriggerEnter (Collider other) {
		if (other.gameObject.tag!="present")
			santaRB.AddForce (0, speed * 50, 0);

		if (other.gameObject.tag == "presentBonus") {
			Destroy (other.gameObject);
			gameManager.presentsCount += 10;
			gameManager.presentsText.text = "Presents: " + gameManager.presentsCount;
			gameManager.presentsBonusCount = 0;

		}
	}
}
