﻿/*
 Скрипт на Санте, "бросающий" подарки
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class throwPresent : MonoBehaviour {

	// Use this for initialization
	public GameObject present, initialPosition; // сам подарок и пустой объект, в позиции которого подарок будет появляться
    public GameObject[] presentArray; // массив подарков, инициализируем из префабов
    int i=0; // счетчик текущего подарка
    public float forceSrengthX = 100; // сила вьроса по оси x
	public float forceSrengthY = 50; // сила вброса по оси y
    public bool canPresent = true; // локальная переменная
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        // раньше бросали только, если предыдущий объект сталкивался с коллайдером
        // теперь бросаем с задержкой
        //	if ( (Input.GetAxis ("Jump")!=0) && (gameManager.canPresent==true)) {
		if ((Input.GetAxis("Jump") != 0) && (canPresent == true) && (gameManager.presentsCount>0))
        {
            // "вбрасываем" подарко в сцене внизу основного действия
            present = Instantiate(presentArray[i], new Vector3(-100, 0, 0), presentArray[i].transform.rotation) ;
			gameManager.presentsCount--;
			gameManager.presentsText.text = "Presents: " + gameManager.presentsCount;
            //present.SetActive(true);
			// если 
			if (gameManager.presentsCount == 4) {
				instantiateElements obmScript = GameObject.Find ("objectsManager").GetComponent<instantiateElements> ();
				obmScript.SetBonusPosition ();
			}
            present.transform.position = initialPosition.transform.position; // ставим подарок в начальную позицию броска
			Rigidbody rgBodyPr = present.gameObject.GetComponent<Rigidbody> (); // получаем компонент Rigidbody
            rgBodyPr.isKinematic = false; // убираем isKinematic
			//rgBodyPr.AddForce (present.transform.right * forceSrength);
			rgBodyPr.AddForce (new Vector3(forceSrengthX,forceSrengthY,0)); // прикладываем силу, про метод читаем в документации
            rgBodyPr.AddTorque(new Vector3(Random.Range(200,900), Random.Range(200, 900), Random.Range(200, 900))); // добавляем вращение, про метод читаем в документации
            canPresent = false; // говорим, что нельзя бросать
            StartCoroutine(waiter()); // вызываем задержку, читаем про задержки в комментариях в скрипте delayEffect
            i++;
            if (i==presentArray.Length)
            {
                i = 0;
            }
           
        }
        
    }

    IEnumerator waiter()
    {
        
        yield return new WaitForSeconds(gameManager.presentDelay); // задержка
        canPresent = true;

    }
}
