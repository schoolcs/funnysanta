﻿/*
    Скрипт, выполняющий глобальные операции (меняем счет, сохраняем данные и т.д.)
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // подключаем методы UI

public class gameManager : MonoBehaviour {
	static public bool canPresent = true; // переменная будет меняться, true - можно кидать подарок, false - нет
	static public int localScore;
	static public int presentsCount;
	static public int presentsBonusCount = 0;
    static public float presentDelay = 0.5f; // задержка перед вбросом очередного подарка
    public Text globalScoreText; // инкапсулирует текстовое UI поле, отображающее глобальный счет
	public Text localScoreText;
	static public Text presentsText;
	// Use this for initialization
	void Start () {
		// 
		presentsText = GameObject.Find("presentsCount").GetComponent<Text>();
		if (PlayerPrefs.HasKey ("presentsCount")) {
			presentsCount = PlayerPrefs.GetInt ("presentsCount");

		} else {
			PlayerPrefs.SetInt ("presentsCount", 10);
			presentsCount = 10;
		}
		presentsText.text = "Presents: " + presentsCount;
		PlayerPrefs.SetInt ("localScore", 0); // устанавливаем текущий счет в 0
		if (PlayerPrefs.HasKey ("globalScore"))
        {
			int globalScore = PlayerPrefs.GetInt ("globalScore"); // считываем значение ключа globalScore
			globalScoreText.text = "Global: " + globalScore.ToString(); // показываем значение счета на экране
            // если есть ключ globalScore
        }
        else {
            // если значения ключа нет, то устанавливаем его в 0
			PlayerPrefs.SetInt ("globalScore", 0);
		}
	}
	
	// Update is called once per frame
	void Update () {
		localScoreText.text = "Score: " + localScore;
	}

	void OnApplicationQuit() { // выполняется до закрытия приложения
        // считываем значения ключей
		int localScore = PlayerPrefs.GetInt ("localScore");
		int globalScore = PlayerPrefs.GetInt ("globalScore");
		if (globalScore < localScore) { // сравниваем, если текущий счет больше глобального - увеличиваем глобальный, записываем значение текущего
			PlayerPrefs.SetInt ("globalScore", localScore);
		}
	}
}
